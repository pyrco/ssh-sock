# ssh-sock

ssh-sock is a simple wrapper script that searches for a usable SSH authentication socket. It has 2 modes, the first is just a simple output of the socket file found, which can then be used in some other script. The second mode executes a given program plus options in an environment with the `SSH_AUTH_SOCK` variable set to the authentication socket file found by the script.

`SSH_AUTH_SOCK` is normally set by some invocation of ssh-agent and subsequent evaluating its output. Generally this happens when logging into an X session or through a shell's interactive .rc file. This script is useful when needing to execute tools that use SSH but the environment this tool is started from has not inherited the `SSH_AUTH_SOCK` variable from its parent. Examples are logins on different VCs, execution of systemd user units etc.


# Usage

When run without any options, the script goes and tries to find a usable SSH authentication socket. It does this in a rather simplistic way, searching `/tmp` for files of the form: `ssh-[a-zA-Z0-9]{12}/agent\.[0-9]+` which are owned by the current user. It checks if there are any SSH keys registered with the ssh-agent on the other end and if so, it will output the file name to standard out.

*Example* print the first SSH authentication socket that has some keys registered for the current user:

```
$ ssh-sock
/tmp/ssh-L3KRGv3JVb5T/agent.2499
```

An option `-u USER` may be specified to search for sockets of the specified user, where `USER` may be a user name or numeric user id.

*Example* print a usable SSH authentication socket for the user with name user:

```
$ ssh-sock -u user
/tmp/ssh-L3KRGv3JVb5T/agent.2499
```

The `-h` and `--help` options will print a short usage message and exit.

When given any other parameters the script will assume these are a program and its parameters and will execute this program in an environment where the `SSH_AuTH_SOCK` variable is set.

*Example* execute an ssh command to connect to example.com on port 2222 as user and with an SSH authentication socket for the user with the name user:

```
$ ssh-sock -u user ssh -p 2222 user@example.com
user@example.com:~$
```
